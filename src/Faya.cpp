#include "plugin.hpp"

#include "Faya/inc/FayaEngine.hpp"

struct Faya : Module {
	enum ParamIds {
		RUN_PARAM,
		MEM_PARAM,
		RESET_PARAM,
		NUM_PARAMS
	};
	enum InputIds {
		CLOCK_INPUT,
		SEQ_INPUT,
		RUN_INPUT,
		RESET_INPUT,
		NUM_INPUTS
	};
	enum OutputIds {
		CV0_OUTPUT,
		G0_OUTPUT,
		ACC0_OUTPUT,
		CV1_OUTPUT,
		G1_OUTPUT,
		ACC1_OUTPUT,
		CV2_OUTPUT,
		G2_OUTPUT,
		ACC2_OUTPUT,
		CV3_OUTPUT,
		G3_OUTPUT,
		ACC3_OUTPUT,
		CV4_OUTPUT,
		G4_OUTPUT,
		ACC4_OUTPUT,
		CV5_OUTPUT,
		G5_OUTPUT,
		ACC5_OUTPUT,
		CV6_OUTPUT,
		G6_OUTPUT,
		ACC6_OUTPUT,
		CV7_OUTPUT,
		G7_OUTPUT,
		ACC7_OUTPUT,
		CV8_OUTPUT,
		G8_OUTPUT,
		ACC8_OUTPUT,
		CV9_OUTPUT,
		G9_OUTPUT,
		ACC9_OUTPUT,
		CV10_OUTPUT,
		G10_OUTPUT,
		ACC10_OUTPUT,
		CV11_OUTPUT,
		G11_OUTPUT,
		ACC11_OUTPUT,
		CV12_OUTPUT,
		G12_OUTPUT,
		ACC12_OUTPUT,
		CV13_OUTPUT,
		G13_OUTPUT,
		ACC13_OUTPUT,
		CV14_OUTPUT,
		G14_OUTPUT,
		ACC14_OUTPUT,
		CV15_OUTPUT,
		G15_OUTPUT,
		ACC15_OUTPUT,
		EOC_OUTPUT,
		NUM_OUTPUTS
	};
	enum LightIds {
		RUN_LIGHT,
		RESET_LIGHT,
		MIDI_LIGHT,
		NUM_LIGHTS
	};

	midi::InputQueue midiInput;
	MidiOutput midiOutput;
	FayaEngine faya;

	dsp::SchmittTrigger clockTrigger;
	dsp::SchmittTrigger runTrigger;
	dsp::SchmittTrigger resetTrigger;
	dsp::Timer timer;
	dsp::ClockDivider screenClock;

	long clockIgnoreOnReset;

	bool midiStatus = false;

	Faya() {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);
		configParam(RUN_PARAM, 0.f, 1.f, 0.f, "Run");
		configParam(MEM_PARAM, 0.f, 63.f, 0.f, "Mem");
		midiOutput.reset();

		screenClock.setDivision(256);

		std::vector<int> ids = midiInput.getDeviceIds();
		for (uint8_t i = 0; i < ids.size(); i++) {
			// DEBUG("%d %s", i, midiInput.getDeviceName(ids[i]).c_str());
			if (midiInput.getDeviceName(ids[i]).compare(0, 13, "FL STUDIO FIRE", 0, 13) == 0) {
				midiInput.setDeviceId(ids[i]);
				DEBUG("MIDI Input set to Akai Fire");
				midiStatus = true;
			}
		}

		ids = midiOutput.getDeviceIds();
		for (uint8_t i = 0; i < ids.size(); i++) {
			//DEBUG("%d %s", i, midiOutput.getDeviceName(ids[i]).c_str());
			if (midiOutput.getDeviceName(ids[i]).compare(0, 13, "FL STUDIO FIRE", 0, 13) == 0) {
				midiOutput.setDeviceId(ids[i]);
				DEBUG("MIDI Output set to Akai Fire");
			}
		}
		
		faya.midiOut = &midiOutput;
		faya.reset();

		onReset();

	}

	void onReset() override {
		DEBUG("onReset");
		clockIgnoreOnReset = (long) (0.001f * APP->engine->getSampleRate()); // useful when Rack starts
		moreReset();
	}

	void moreReset() {
		DEBUG("moreReset");
		faya.currentMemory = int(params[MEM_PARAM].getValue());
		faya.init();
		if (midiStatus) {
			lights[MIDI_LIGHT].setBrightness(1.0);
		}
		DEBUG("MEMORY %d", faya.currentMemory);
	}
	
	uint8_t voltageToMem(float volt) {
		return std::floor(int(volt/0.153846154));
	}

	float tickAccumulator;
	void process(const ProcessArgs& args) override {

		// Timer for blinks?
		timer.process(args.sampleTime);

		// MEM knob is disabled (this code updates the param from the current value)
/*		if (int(params[MEM_PARAM].getValue()) != faya.currentMemory) {
			params[MEM_PARAM].setValue(float(faya.currentMemory));
		}*/

		// TODO: change to timer. Used to time the view on the controller
		tickAccumulator += args.sampleTime;
		if (tickAccumulator > faya.view.uiTick) {
			faya.view.tick();
			tickAccumulator = 0.0;
		}
		midi::Message msg;
		while (midiInput.shift(&msg)) {
			//DEBUG("MIDI: %01x %01x %02x %02x", msg.getStatus(), msg.getChannel(), msg.getNote(), msg.getValue());
			faya.handle(msg);
		}

		// Run
		if (runTrigger.process(params[RUN_PARAM].getValue() + inputs[RUN_INPUT].getVoltage())) {
			faya.running = !faya.running;
			faya.view.lightTransport(faya.running);
			// Reset on run here if enabled (not implemented)
		}	

		// Reset
		resetTrigger.process(inputs[RESET_INPUT].getVoltage() + params[RESET_PARAM].getValue());

		
		uint8_t _mem = voltageToMem(inputs[SEQ_INPUT].getVoltage());
		if (inputs[SEQ_INPUT].isConnected() && _mem != (faya.view.detached ? faya.currentPlayMemory : faya.currentMemory)) {
			if (_mem < 64) {
				// Load only the seq, not the view
				DEBUG("new mem: %d", _mem);
				faya.loadMemory(_mem, false);
			}
		}

		uint8_t clock = clockTrigger.process(inputs[CLOCK_INPUT].getVoltage());

		if (faya.running && clockIgnoreOnReset == 0) { // clock muting and 1ms-clock-ignore-on-reset
			bool gateIn = false;
			if (clock) {
				faya.step();
			} else {

			}

			gateIn = clockTrigger.isHigh();

			// Handle detached view with an if
			for (uint8_t g = 0; g < SEQ_TRACKS; g++) {
				if (!faya.view.detached) {
					outputs[g*3+1].setVoltage((gateIn && faya.seq.seqs[g][faya.index] && faya.trackState(g) && (clockIgnoreOnReset == 0)) ? 10.f : 0.f); // gate retriggering on reset
					outputs[g*3+2].setVoltage((gateIn && faya.seq.accents[g][faya.index] && faya.trackState(g) && (clockIgnoreOnReset == 0)) ? 10.f : 0.f); // gate retriggering on reset
				} else {		
					outputs[g*3+1].setVoltage((gateIn && faya.playSeq.seqs[g][faya.index] && faya.trackState(g) && (clockIgnoreOnReset == 0)) ? 10.f : 0.f); // gate retriggering on reset
					outputs[g*3+2].setVoltage((gateIn && faya.playSeq.accents[g][faya.index] && faya.trackState(g) && (clockIgnoreOnReset == 0)) ? 10.f : 0.f); // gate retriggering on reset
				}
			}

			outputs[EOC_OUTPUT].setVoltage(faya.EOC ? 10.f : 0.f);
		}

		// Reset
		if (resetTrigger.isHigh()) {
			faya.resetSeq();
			clockIgnoreOnReset = (long) (0.001f * args.sampleRate);
			clockTrigger.reset();
		}
		

		for (uint8_t cv = 0; cv < SEQ_TRACKS; cv++) {
			outputs[cv*3].setChannels(4);
			outputs[cv*3].setVoltage(faya.polyCV[cv * 4], 0);
			outputs[cv*3].setVoltage(faya.polyCV[cv * 4 + 1], 1);
			outputs[cv*3].setVoltage(faya.polyCV[cv * 4 + 2], 2);
			outputs[cv*3].setVoltage(faya.polyCV[cv * 4 + 3], 3);
		}

		lights[RUN_LIGHT].value = faya.running;

		if (screenClock.process()) {
			if (faya.view.markUpdateScreen) {
				faya.view.updateScreen();
				faya.view.markUpdateScreen = false;
			}
		}

		if (clockIgnoreOnReset > 0l) {
			clockIgnoreOnReset--;
		}
	}

	json_t* dataToJson() override {
		faya.memory[faya.currentMemory] =  faya.seq;
		json_t* rootJ = json_object();

		// running
		json_object_set_new(rootJ, "running", json_boolean(faya.running));

		// memory
		json_t* memoryJ = json_array();
		std::string encoded;
		for (int i = 0; i < 64; i++) {
			if (faya.memory[i].used) {
				encoded = faya.encodeMemory(i);
				json_array_insert_new(memoryJ, i, json_stringn(encoded.c_str(), encoded.size()));
			}
		}
		json_object_set_new(rootJ, "memory", memoryJ);

		return rootJ;
	}

	void dataFromJson(json_t* rootJ) override {
		// running
		json_t* runningJ = json_object_get(rootJ, "running");
		if (runningJ)
			faya.running = json_is_true(runningJ);

		// memory 
		json_t* memoryJ = json_object_get(rootJ, "memory");
		size_t len_memory = json_array_size(memoryJ);
		if (memoryJ) {
			for (size_t i = 0; i < len_memory; i++) {
				json_t* memJ = json_array_get(memoryJ, i);
				if (memJ) {
					size_t len = json_string_length(memJ);
					const char* memStr = json_string_value(memJ);
					faya.decodeMemory(i, memStr, len);
					faya.memory[i].used = true;
				}
			}
		}

		moreReset();
		DEBUG("dataFromJson");
	}
};

struct MemoryDisplay : LedDisplayTextField {
	
	MemoryDisplay() {
		textOffset = Vec(0.1,-1.);
		LedDisplayTextField();
	}
};

struct RoundSmallSnapBlackKnob : RoundSmallBlackKnob {
	RoundSmallSnapBlackKnob() {
		snap = true;
		RoundSmallBlackKnob();
	}
};

struct FayaMarkSeqItem : MenuItem {
	Faya* faya;
	void onAction(const event::Action& e) override {
		faya->faya.view.markSeq ^= true;
		// TODO: don't do that from here!
		faya->faya.view.updateView(faya->faya.index);
	}
	void step() override {
		rightText = CHECKMARK(faya->faya.view.markSeq);
	}
};

struct FayaClampItem : MenuItem {
	Faya* faya;
	void onAction(const event::Action& e) override {
		faya->faya.seq.clampNotes ^= true;
	}
	void step() override {
		rightText = CHECKMARK(faya->faya.seq.clampNotes);
	}
};

struct FayaWidget : ModuleWidget {
	
	MemoryDisplay* textField;

	void step() override {
		if (module) {
			Faya* faya = dynamic_cast<Faya*>(module);	
			textField->setText(std::to_string(faya->faya.currentMemory + 1));
		}
		ModuleWidget::step();
	}

	FayaWidget(Faya* module) {
		setModule(module);
		setPanel(APP->window->loadSvg(asset::plugin(pluginInstance, "res/Faya.svg")));

		addParam(createParamCentered<LEDButton>(mm2px(Vec(46.7, 19.5)), module, Faya::RUN_PARAM));
		addChild(createLightCentered<MediumLight<YellowLight>>(mm2px(Vec(46.7, 19.5)), module, Faya::RUN_LIGHT));

		addParam(createParamCentered<LEDButton>(mm2px(Vec(36.7, 19.5)), module, Faya::RESET_PARAM));
		addChild(createLightCentered<MediumLight<YellowLight>>(mm2px(Vec(36.7, 19.5)), module, Faya::RESET_LIGHT));

		addChild(createLightCentered<MediumLight<GreenLight>>(mm2px(Vec(53, 8)), module, Faya::MIDI_LIGHT));
	
		//addParam(createParamCentered<RoundSmallSnapBlackKnob>(mm2px(Vec(17., 32.6)), module, Faya::MEM_PARAM));
		// addInput(createInputCentered<PJ301MPort>(mm2px(Vec(6.928, 32.6)), module, Faya::MEM_INPUT));

		textField = createWidget<MemoryDisplay>(mm2px(Vec(13.5, 26.8-3.0)));
		textField->box.size = mm2px(Vec(6.9, 6.0));
		textField->multiline = false;
		addChild(textField);

		addInput(createInputCentered<PJ301MPort>(mm2px(Vec(6.652, 26.817)), module, Faya::SEQ_INPUT));
		addInput(createInputCentered<PJ301MPort>(mm2px(Vec(6.667, 122.019)), module, Faya::CLOCK_INPUT));
		addInput(createInputCentered<PJ301MPort>(mm2px(Vec(22.542, 122.019)), module, Faya::RUN_INPUT));
		addInput(createInputCentered<PJ301MPort>(mm2px(Vec(38.417, 122.019)), module, Faya::RESET_INPUT));

		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 43.487)), module, Faya::CV0_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 43.487)), module, Faya::CV8_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 43.487)), module, Faya::G0_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 43.487)), module, Faya::G8_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 43.487)), module, Faya::ACC0_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 43.487)), module, Faya::ACC8_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 52.018)), module, Faya::CV1_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 52.018)), module, Faya::CV9_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 52.018)), module, Faya::G1_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 52.018)), module, Faya::G9_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 52.018)), module, Faya::ACC1_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 52.018)), module, Faya::ACC9_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 60.55)), module, Faya::CV2_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 60.55)), module, Faya::CV10_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 60.55)), module, Faya::G2_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 60.55)), module, Faya::G10_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 60.55)), module, Faya::ACC2_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 60.55)), module, Faya::ACC10_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 69.082)), module, Faya::CV3_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 69.082)), module, Faya::CV11_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 69.082)), module, Faya::G3_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 69.082)), module, Faya::G11_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 69.082)), module, Faya::ACC3_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 69.082)), module, Faya::ACC11_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 77.554)), module, Faya::CV4_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 77.554)), module, Faya::CV12_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 77.554)), module, Faya::G4_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 77.554)), module, Faya::G12_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 77.554)), module, Faya::ACC4_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 77.554)), module, Faya::ACC12_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 86.086)), module, Faya::CV5_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 86.086)), module, Faya::CV13_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 86.086)), module, Faya::G5_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 86.086)), module, Faya::G13_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 86.086)), module, Faya::ACC5_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 86.086)), module, Faya::ACC13_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 94.617)), module, Faya::CV6_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 94.617)), module, Faya::CV14_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 94.617)), module, Faya::G6_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 94.617)), module, Faya::G14_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 94.617)), module, Faya::ACC6_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 94.617)), module, Faya::ACC14_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(6.928, 103.149)), module, Faya::CV7_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.394, 103.149)), module, Faya::CV15_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(27.565, 103.149)), module, Faya::G7_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(36.032, 103.149)), module, Faya::G15_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(46.615, 103.149)), module, Faya::ACC7_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(55.082, 103.149)), module, Faya::ACC15_OUTPUT));
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(54.292, 122.019)), module, Faya::EOC_OUTPUT));
	}

	void appendContextMenu(Menu* menu) override {
		menu->addChild(new MenuEntry);

		Faya* faya = dynamic_cast<Faya*>(module);
		assert(faya);

		FayaClampItem* clampItem = createMenuItem<FayaClampItem>("Clamp CV to notes");
		clampItem->faya = faya;
		menu->addChild(clampItem);

		FayaMarkSeqItem* markSeqItem = createMenuItem<FayaMarkSeqItem>("Mark toggled steps in CV");
		markSeqItem->faya = faya;
		menu->addChild(markSeqItem);
	}
};


Model* modelFaya = createModel<Faya, FayaWidget>("Faya");
