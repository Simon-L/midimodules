#pragma once
#include <rack.hpp>


using namespace rack;

// TODO: this has nothing to do here! move it to a separate file that abstracts it for the Faya engine
struct MidiOutput : midi::Output {
	void reset() {
		Output::reset();
	}
};

// Declare the Plugin, defined in plugin.cpp
extern Plugin* pluginInstance;

// Declare each Model, defined in each module source file
extern Model* modelFaya;
