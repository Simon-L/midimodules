#include "inc/View.hpp"
#include "inc/Constants.hpp"
#include "inc/Utils.hpp"
#include "inc/Bitmap.h"
#include <bits/stdint-uintn.h>

void Screen::clear(uint8_t fromX, uint8_t toX, uint8_t fromY, uint8_t toY) {
	for (uint8_t x = fromX; x < toX; ++x) {
		for (uint8_t y = fromY; y < toY; ++y) {
			PlotPixel(x, y, 0);
		}
	}
}

void Screen::clear() {
	for (uint8_t x = 0; x < 128; ++x) {
		for (uint8_t y = 0; y < 64; ++y) {
			PlotPixel(x, y, 0);
		}
	}
	DEBUG("clear");
}

void Screen::drawScale() {
	for (uint8_t h = 28; h < 33; h++) {
		for (uint8_t x = 0; x < 128; x++) {
			if (h == 28 || x == 127) PlotPixel(x, h, 1);
			else PlotPixel(x, h, (x%13 == 0 ? 1 : 0));
		}
	}
}

void Screen::drawValue(float value) {
	uint8_t threshold = int(std::roundf(((value+5.0)/10.0) * 128.0));
	for (uint8_t x = 0; x < 128; ++x) {
		PlotPixel(x, 33, 0);
		for (uint8_t h = 34; h < 34+10; h++) {
			if (x < threshold) PlotPixel(x, h, 1);
			else PlotPixel(x, h, 0);
		}
	}
}

void Screen::drawChar(char c, uint8_t x, uint8_t y) {
	uint8_t i = c - 32;
	if (i > 94) return;
	for (uint8_t h = 0; h < 13; h++) {
		for (uint8_t w = 0; w <= 8; w++) {
			PlotPixel(x + w, y + 13 - h, (letters[i][h] & 1 << (8-w) ? 1 : 0));
		}
	}
}

void Screen::drawNote(int note) {
	drawChar(noteToChar(note), 5, 5);
	switch (note % 12) {
		case 1:
		case 6:
		case 8:
			drawChar('#', 5+9, 5);
			break;
		case 3:
		case 10:
			drawChar('b', 5+9, 5);
			break;
		default:
			drawChar(' ', 5+9, 5);
			break;
	}
	char oct = (note / 12) + 48 - 1;
	if (oct == 47) oct = '-';
	drawChar(oct, 5+9+9, 5);
}

void Screen::drawString(const char *str, uint8_t size, uint8_t x, uint8_t y) {
	for (int i = 0; i < size; i++) {
		drawChar(str[i], x, y);
		x += 9;
	}
}

void Screen::drawMemory(uint8_t id) {
	id += 1;
	drawString("Mem:", 4, 74, 5);
	drawChar((id / 10) + 48, 110, 5);
	drawChar((id % 10) + 48, 119, 5);
	DEBUG("draw");
}

void Screen::drawStep(uint8_t index, uint8_t length) {
	index += 1;
	drawChar('/', 101, 19);
	if (length < 10) {
		drawChar(length + 48, 110, 19);
		drawChar(' ', 119, 19);
	} else {
		drawChar((length / 10) + 48, 110, 19);
		drawChar((length % 10) + 48, 119, 19);
	}
	if (index < 10) {
		drawChar(index + 48, 92, 19);
		drawChar(' ', 83, 19);
	} else {
		drawChar((index / 10) + 48, 83, 19);
		drawChar((index % 10) + 48, 92, 19);
	}

}

void Screen::drawTrack(int8_t track) {
	track += 1;
	drawString("Track ", 6, 0, 5);
	drawChar((track / 10) + 48, 50, 5);
	drawChar((track % 10) + 48, 59, 5);
}

void Screen::drawKnob(float value, uint8_t x) {
	if (value == 10.0) {
		drawChar('1',  x,  50);
		drawChar('0',  x+9,  50);
		PlotPixel(    x+19, 63, 1);
		PlotPixel(    x+19, 62, 1);
		drawChar('0', x+20, 50);
	} else {
		uint8_t dec = (uint8_t)(trunc(value));
		uint8_t frac = (uint8_t)((value - trunc(value)) * 100);
		drawChar(dec + 48, x, 50);
		PlotPixel(    x+10, 63, 1);
		PlotPixel(    x+10, 62, 1);
		drawChar((frac / 10) + 48, x+11, 50);
		drawChar((frac % 10) + 48, x+20, 50);
	}
}

View::View() {

}

void View::reset(uint8_t memory) {

	shifted = false;
	trackPage = TRACK_PAGE_1;
	page = PAGE_1;
	hasSelected = false;
	blink = false;
	DEBUG("reset clear");
	Screen::clear();
	DEBUG("reset updated");

	switchMode(STEP);
	lightMode();
	lightTrackPage();
	lightPage();
	lightTransport(0);
};

void View::updateView(uint8_t index) {
	switch(this->mode) {
		case STEP:
		case ACCENT:
			showPageSeqs(index);
			break;
	}
}

void View::displayMemory(uint8_t id) {
	DEBUG("displayMemory, %d", id);
	Screen::drawMemory(id);
	markUpdateScreen = true;
}

void View::displayTrack() {
	Screen::drawTrack(trackSel);
	markUpdateScreen = true;

}

void View::displayStep(uint8_t index, uint8_t length) {
	Screen::drawStep(index, length);
	markUpdateScreen = true;
}

void View::displayKnobs(float k1, float k2, float k3, float k4) {
	Screen::clear(0, 128, 51, 64);
	Screen::drawKnob(k1, 0);
	Screen::drawKnob(k2, 33);
	Screen::drawKnob(k3, 66);
	Screen::drawKnob(k4, 99);
	markUpdateScreen = true;
}

void View::pagePad(Pad *pad) {
	pad->track = (trackPage * 4 + 3) - pad->y;
	pad->position = page * 16 + pad->x;
	DEBUG("PAD track:%d pos:%d", pad->track, pad->position);
}

void View::lightPad(Pad pad){
	std::vector<unsigned char> m;
	PadSysEx(pad.x, pad.y, pad.color, &m);
	midiOut->sendMessage(m.data(), m.size());
}

void View::lightMultiplePads(std::vector<Pad> pads){
	std::vector<unsigned char> m;
	for (uint8_t i = 0; i < pads.size(); ++i)
	{
		SinglePadSysEx(pads[i].x, pads[i].y, pads[i].color, &m);
	}
	FinishSysEx(&m);
	midiOut->sendMessage(m.data(), m.size());
}

void View::clearPads() {
	std::vector<Pad> pads;
	for (uint8_t x = 0; x < 16; x++)
	{
		for (uint8_t y = 0; y < 4; y++) {
			pads.push_back(Pad{x,y,0x0000});
		}
	}
	lightMultiplePads(pads);
}

void View::lightTrackPage() {
	m.setStatus(0xB);
	switch (this->trackPage) {
		case TRACK_PAGE_1:
			m.setNote(PAT_DO_BUT);
			m.setValue(2);
			midiOut->sendMessage(m);
			m.setNote(PAT_UP_BUT);
			m.setValue(0);
			midiOut->sendMessage(m);
			break;
		case TRACK_PAGE_2:
			m.setNote(PAT_DO_BUT);
			m.setValue(2);
			midiOut->sendMessage(m);
			m.setNote(PAT_UP_BUT);
			m.setValue(1);
			midiOut->sendMessage(m);
			break;
		case TRACK_PAGE_3:
			m.setNote(PAT_DO_BUT);
			m.setValue(1);
			midiOut->sendMessage(m);
			m.setNote(PAT_UP_BUT);
			m.setValue(2);
			midiOut->sendMessage(m);
			break;
		case TRACK_PAGE_4:
			m.setNote(PAT_DO_BUT);
			m.setValue(0);
			midiOut->sendMessage(m);
			m.setNote(PAT_UP_BUT);
			m.setValue(2);
			midiOut->sendMessage(m);
			break;
		default:
			break;
	}
}

void View::lightPage() {
	m.setStatus(0xB);
	switch (this->page) {
		case PAGE_1:
			m.setNote(GRID_L);
			m.setValue(2);
			midiOut->sendMessage(m);
			m.setNote(GRID_R);
			m.setValue(0);
			midiOut->sendMessage(m);
			break;
		case PAGE_2:
			m.setNote(GRID_L);
			m.setValue(2);
			midiOut->sendMessage(m);
			m.setNote(GRID_R);
			m.setValue(1);
			midiOut->sendMessage(m);
			break;
		case PAGE_3:
			m.setNote(GRID_L);
			m.setValue(1);
			midiOut->sendMessage(m);
			m.setNote(GRID_R);
			m.setValue(2);
			midiOut->sendMessage(m);
			break;
		case PAGE_4:
			m.setNote(GRID_L);
			m.setValue(0);
			midiOut->sendMessage(m);
			m.setNote(GRID_R);
			m.setValue(2);
			midiOut->sendMessage(m);
			break;
		default:
			break;
	}
}

bool View::switchMode(ViewMode mode) {
	if (this->mode == mode) return false;
	else {
		this->mode = mode;
		lightMode();
		hasSelected = false;
		return true;
	}
}

void View::setBrowsing(bool state) {
	browsing = state;
	m.setStatus(0xb);
	m.setNote(BRO_BUT);
	m.setValue(state ? 2 : 0);
	midiOut->sendMessage(m);

	if (browsing) { // Dim red for grid buttons when browsing
		m.setStatus(0xb);
		m.setNote(GRID_L);
		m.setValue(1);
		midiOut->sendMessage(m);	
		m.setStatus(0xb);
		m.setNote(GRID_R);
		m.setValue(1);
		midiOut->sendMessage(m);
	} else {
		lightPage();
	}

}

void View::lightMode() {
	switch (this->mode) {
		case STEP:
		case ACCENT:
			m.setStatus(0xb);
			m.setNote(STEP_BUT);
			m.setValue(this->mode == STEP ? 1 : 4);
			midiOut->sendMessage(m);
			// m.setNote(NOTE_BUT);
			// m.setValue(0);
			// midiOut->sendMessage(m);
			m.setNote(PERF_BUT);
			m.setValue(0);
			midiOut->sendMessage(m);
			break;
		case PERFORM:
			m.setNote(STEP_BUT);
			m.setValue(0);
			midiOut->sendMessage(m);
			m.setNote(PERF_BUT);
			m.setValue(4);
			midiOut->sendMessage(m);

	}
}

void View::lightTransport(bool play) {
	m.setStatus(0xb);
	m.setNote(PLAY_BUT);
	m.setValue(play ? 3 : 0);
	midiOut->sendMessage(m);
	m.setNote(STOP_BUT);
	m.setValue(play ? 0 : 1);
	midiOut->sendMessage(m);

}

bool View::moveTrackPage(bool direction) {
/*	if ((trackPage == BANK_UPPER) && (to == BANK_LOWER)) {
		trackPage = BANK_LOWER;
		lightTrackPage();
		hasSelected = false;
		return true;
	}
	if ((trackPage == BANK_LOWER) && (to == BANK_UPPER)) {
		trackPage = BANK_UPPER;
		lightTrackPage();
		hasSelected = false;
		return true;
	}
	return false;*/
	// increment page
	if (direction && trackPage + 1 <= TRACK_PAGE_4) {
		trackPage++;
		lightTrackPage();
		// has selected?
		DEBUG("UP %d", trackPage);
		return true;
	}
	// decrement page
	if (!direction && trackPage - 1 >= TRACK_PAGE_1) {
		trackPage--;
		lightTrackPage();
		DEBUG("DOWN %d", trackPage);
		//has selected ?
		return true;
	}
	return false;
}

bool View::movePage(bool direction) {
	if (mode == View::PERFORM) return false;
	// increment page
	if (direction && page + 1 <= PAGE_4) {
		page++;
		lightPage();
		// has selected?
		return true;
	}
	// decrement page
	if (!direction && page - 1 >= PAGE_1) {
		page--;
		lightPage();
		//has selected ?
		return true;
	}
	return false;
}

void View::lightDetached() {
	m.setStatus(0xb);
	m.setNote(PAT_BUT);
	m.setValue(detached ? 4 : 0);
	midiOut->sendMessage(m);
}

void View::lightPerformClk(uint8_t index) {
	std::vector<Pad> clkPads;
	for (int x = 0; x < 16; x++) {
		if (x == (index % 16)) clkPads.push_back(Pad{x,0,CLK_COLOR});
		else if (seq->seqs[x][index]) clkPads.push_back(Pad{x,0,SEQ_STEP_COLOR});
		else clkPads.push_back(Pad{x,0,0x000000});
	}
	lightMultiplePads(clkPads);
}

void View::moveClock(uint8_t fromStep, uint8_t toStep) {
	if (mode == View::PERFORM) {
		lightPerformClk(toStep);
		return;
	}
	std::vector<unsigned char> m;
	uint32_t fromSeq[4];
	// offset view for upper trackPage 
	uint8_t trackOffset = this->trackPage * 4;
	for (unsigned i = 0; i < 4; i++) {
		switch (mode) {
			case STEP:
			case ACCENT:
				if (seq->accents[trackOffset + i][fromStep])
					fromSeq[i] = SEQ_ACC_COLOR;
				else
					fromSeq[i] =
						(seq->seqs[trackOffset + i][fromStep] ? SEQ_STEP_COLOR
						 : 0x000000);
				break;
		}
	}
	if (fromStep != NO_STEP) ColSysEx(fromStep % 16, fromSeq, &m);
	if (toStep != NO_COL) ClkSysEx(toStep % 16, CLK_COLOR, &m);
	FinishSysEx(&m);
	midiOut->sendMessage(m.data(), m.size());
}

bool View::indexInView(uint8_t clockIndex) {
	if (clockIndex < (this->page * 16 + 16) && clockIndex >= this->page * 16) {
		return true;
	}
	else return false;
}

void View::showPerform(uint8_t clockIndex, uint16_t solo, uint16_t mute) {
	clearPads();
	std::vector<Pad> pads;
	for (uint8_t x = 0; x < 16; x++) {
		pads.push_back(Pad{x,3,(x == trackSel) ? PERFORM_SELECT_COLOR : PERFORM_SELECT_COLOR_DIM});
		pads.push_back(Pad{x,2,((mute & (1 << x)) ? PERFORM_MUTE_COLOR : PERFORM_MUTE_COLOR_DIM)});
		pads.push_back(Pad{x,1,((solo & (1 << x)) ? PERFORM_SOLO_COLOR : PERFORM_SOLO_COLOR_DIM)});
	}
	lightMultiplePads(pads);
}

void View::showPageSeqs(uint8_t clockIndex) {
	showPageSeqs(seq->seqs, clockIndex);
}
void View::showPageSeqs(std::vector<uint8_t> *seqs, uint8_t clockIndex) {
	uint8_t trackOffset = this->trackPage * 4;
	std::vector<unsigned char> m;
	uint32_t colSeq[4];
	for (int step = this->page * 16; step <= this->page * 16 + 15; step++) {
		if (step == clockIndex) {
			ClkSysEx(step % 16, CLK_COLOR, &m);
		}
		if (step >= seq->length[0]) {
			ClkSysEx(step % 16, BLANK_COLOR, &m);
		} else {
			for (int t = 0; t < 4; t++) {
				if (seq->accents[trackOffset + t][step] == 1) colSeq[t] = SEQ_ACC_COLOR;
				else colSeq[t] = (seqs[trackOffset + t][step] ? SEQ_STEP_COLOR : 0x000000);
			}
			ColSysEx(step % 16, colSeq, &m);
		}
	}
	FinishSysEx(&m);
	midiOut->sendMessage(m.data(), m.size());

}

void View::showPage(uint8_t clockIndex) {
	if (mode == STEP || mode == ACCENT) showPageSeqs(clockIndex);
}

bool View::selectStep(Pad *step) {
	// NOTE: Unused because CV sequence is disabled, kept if steps need to be selected in the future!
	// if (step->position == selected.position && step->track == selected.track && hasSelected) return false;
	// pagePad(step);
	// if (hasSelected) {
	// 	//revert previously selected to cv color
	// 	selected.color = cvToColor(seq->cvs[selected.track][selected.position]);
	// 	// TODO: idem repeated from showPageCv
	// 	if (markSeq && seq->seqs[selected.track][selected.position]) selected.color |= CV_STEPUNDERLAY_COLOR;
	// 	if (markSeq && seq->accents[selected.track][selected.position]) selected.color |= CV_STEPACCUNDERLAY_COLOR;
	// 	//light it
	// 	lightPad(selected);
	// }
	// /*set newly selected to selected color*/
	// step->color = SELECT_COLOR;
	// // light it
	// lightPad(*step);
	// /*set new selected */
	// selected = *step;
	// hasSelected = true;
	// DEBUG("Selected is: %d %d", selected.track, selected.position);
	return true;
}


void View::tick() {
	// See selectStep
	// switch(mode) {
	// 	case CV:
	// 		blink = !blink;
	// 		if (hasSelected) {
	// 			Pad tmp = selected;
	// 			if (blink) {
	// 				tmp.color = SELECT_COLOR;
	// 				lightPad(tmp);
	// 			} else {
	// 				tmp.color = cvToColor(
	// 						seq->cvs[selected.track][selected.position]);
	// 				// TODO: idem repeated from showPageCv
	// 				if (markSeq && seq->seqs[selected.track][selected.position]) tmp.color |= CV_STEPUNDERLAY_COLOR;
	// 				if (markSeq && seq->accents[selected.track][selected.position]) tmp.color |= CV_STEPACCUNDERLAY_COLOR;
	// 				lightPad(tmp);
	// 			}
	// 		}
	// 		break;
	// }
}



void View::updateScreen() {
	screenBuffer.clear();
	OledSysex(OLEDBitmap, sizeof(OLEDBitmap), &screenBuffer);
	midiOut->sendMessage(screenBuffer.data(), screenBuffer.size());
	//DEBUG("update");
}
