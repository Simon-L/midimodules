#pragma once

#include <cstdint>
#include <vector>
#include <string>

#include "../../plugin.hpp"

#include "Constants.hpp"
#include "Seq.hpp"
#include "View.hpp"

struct FayaEngine {

	MidiOutput* midiOut;

	FayaEngine(); 

	~FayaEngine();

	void reset();
	void init();

	uint8_t index = 0;
	uint8_t numSteps = 15;
	bool resetIndex = true;
	bool EOC = false;
	bool running = false;
	uint8_t currentMemory = 0;
	uint8_t currentPlayMemory = 0;

	uint8_t nextSeq = NO_NEXT_SEQ;
	
	Seq seq;
	Seq playSeq;
	Memory memory[64];
	View view;
	// One track per bit
	uint16_t tracksSolo = 0b0000000000000000;
	uint16_t tracksMute = 0b0000000000000000;

	void loadMemory(uint8_t id, bool update = false);
	void resetSeq();

	midi::Message msg;
	void lightMutes();
	void lightSelect(bool disable = false);
	void lightTrackLeds(uint8_t index, bool off);

	bool toggleTrackMute(uint8_t track);

	bool toggleTrackSolo(uint8_t track);
	
	bool trackState(uint8_t track);

	void lightClk(uint8_t step);

	Pad chose;
	void step();

	std::vector<float> polyCV;

	float increment = 0.25;
	void display();	
	std::vector<uint8_t> buf;
	std::string decoded;
	std::string encodeMemory(uint8_t id);
	void decodeMemory(uint8_t id, const char* str, size_t len);
	
	float noteSelectorAcc = 0.0;
	uint8_t noteSelector = 0;
	void handle(midi::Message msg);
};
