#pragma once


//////////////
// SEQUENCE //
//////////////

#define SEQ_TRACKS 16

////////////
// COLORS //
////////////

#define SEQ_STEP_COLOR 0xBB0000
#define SEQ_ACC_COLOR 0xFF3300
#define CLK_COLOR 0x00FF00
#define BLANK_COLOR 0x111111
#define SELECT_COLOR 0x000000
#define CV_STEPUNDERLAY_COLOR 0x330000
#define PERFORM_SELECT_COLOR 0x888800
#define PERFORM_SELECT_COLOR_DIM 0x333300
#define PERFORM_MUTE_COLOR 0xFF7800
#define PERFORM_MUTE_COLOR_DIM 0x442800
#define PERFORM_SOLO_COLOR 0x78FF11
#define PERFORM_SOLO_COLOR_DIM 0x284400


///////
// Other defines
//////

#define NO_COL 65
#define NO_STEP 65

#define NO_NEXT_SEQ 65

//////////
// MIDI //
//////////


// Midi type
// WARNING this is only the status part of the byte, don't forget!
#define BUT_ON 0x9
#define BUT_OFF 0x8
#define KNOB 0xb

// CC
#define VOL_KNOB 0x10
#define PAN_KNOB 0x11
#define LOEQ_KNOB 0x12
#define HIEQ_KNOB 0x13

#define SELECT_KNOB 0x76

// Notes
#define SELECT_BUT 0x19

#define MIXER_BUT 0x1A

// (Mixer leds
#define MIXER_LED 0x1b
// )

#define PAT_UP_BUT 0x1F
#define PAT_DO_BUT 0x20

#define BRO_BUT 0x21

#define GRID_L 0x22
#define GRID_R 0x23

#define TRACK_3_BUT 0x24
#define TRACK_2_BUT 0x25
#define TRACK_1_BUT 0x26
#define TRACK_0_BUT 0x27

// ( Lanes leds
#define TRACK_3_LED 0x28
#define TRACK_2_LED 0x29
#define TRACK_1_LED 0x2a
#define TRACK_0_LED 0x2b
// )

#define STEP_BUT 0x2c
#define NOTE_BUT 0x2d
#define DRUM_BUT 0x2e
#define PERF_BUT 0x2f

#define SHIFT_BUT 0x30
#define ALT_BUT 0x31

#define PAT_BUT 0x32
#define PLAY_BUT 0x33
#define STOP_BUT 0x34
#define REC_BUT 0x35

#define FIRST_STEP 0x36
#define LAST_STEP 0x75

// SysEx


//Red-only 	    Green-only 	        Yellow-only     	Yellow-red 	        Yellow-green
//PAT BACK 	    SOLO 1 	             ALT 	            STEP 	            PATTERN
//PAT NEXT 	    SOLO 2 	             STOP 	            NOTE 	            PLAY
//BROWSER 	    SOLO 3 		                            DRUM
//GRID LEFT 	    SOLO 4 		                            PERFORM
//GRID RIGHT 			                                    SHIFT 	
//								    LOOP REC
//Control values
//00 – Off 	    00 – Off 	        00 – Off 	        00 – Off 	        00 – Off
//01 – Dull Red 	01 – Dull Green 	01 – Dull Yellow 	01 – Dull Red 	01 – Dull Yellow
//02 – High Red 	02 – High Green 	02 – High Yellow 	02 – Dull Yellow 	    02 – Dull Green
//			                                            03 – High Red 	03 – High Yellow
//			                                            04 – High Yellow 	    04 – High Green
//
