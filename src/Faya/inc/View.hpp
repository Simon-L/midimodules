#pragma once

#include <bits/stdint-uintn.h>
#include <vector>

#include "../../plugin.hpp"

#include "Constants.hpp"
#include "Types.hpp"
#include "Seq.hpp"

#define BANK_LOWER 0
#define BANK_UPPER 1

#define TRACK_PAGE_1 0
#define TRACK_PAGE_2 1
#define TRACK_PAGE_3 2
#define TRACK_PAGE_4 3

#define PAGE_1 0
#define PAGE_2 1
#define PAGE_3 2
#define PAGE_4 3

struct Screen {
	static void clear(uint8_t fromX, uint8_t toX, uint8_t fromY, uint8_t toY);
	static void clear(); 
	static void drawScale();
	static void drawValue(float value);
	static void drawNote(int note);
	static void drawChar(char c, uint8_t x, uint8_t y);
	static void drawString(const char *str, uint8_t size, uint8_t x, uint8_t y);
	static void drawMemory(uint8_t id);
	static void drawStep(uint8_t index, uint8_t length);
	static void drawTrack(int8_t track);
	static void drawKnob(float knobs, uint8_t x);
};

struct View {
	MidiOutput* midiOut;

	float uiTick = 0.3;
	
	bool shifted;
	bool browsing = false;

    uint8_t trackPage;
	uint8_t page;
	int8_t trackSel = 0;

    enum ViewMode {
		STEP,
		ACCENT,
		PERFORM,
	};
	ViewMode mode;
	Pad selected;
	bool hasSelected;
	Seq *seq;
	bool detached = false;

	// In CV view, mark steps that are toggled/accented with a slight red/orange underlay
	bool markSeq = true;

	View();

	void reset(uint8_t memory);

	void updateView(uint8_t index);
	void displayMemory(uint8_t id);
	void displayStep(uint8_t index, uint8_t length);
	void displayTrack();
	void displayKnobs(float k1, float k2, float k3, float k4);
	
	void pagePad(Pad* pad);
	
	void lightPad(Pad pad);
	void lightMultiplePads(std::vector<Pad> pads);
	void lightPerformClk(uint8_t index);

	void clearPads();
	midi::Message m;
	void lightTrackPage(); 
	void lightPage(); 
	void lightDetached(); 

	bool switchMode(ViewMode mode);

	void setBrowsing(bool state);

	void lightMode();


	void lightTransport(bool play);

	bool moveTrackPage(bool direction);
	bool movePage(bool direction);

	void moveClock(uint8_t fromStep, uint8_t toStep = NO_COL);

	bool indexInView(uint8_t clockIndex);
	void showPerform(uint8_t clockIndex, uint16_t solo, uint16_t mute);
	void showPageSeqs(uint8_t clockIndex);
	void showPageSeqs(std::vector<uint8_t> *seqs, uint8_t clockIndex);

	void showPage(uint8_t clockIndex);

	bool selectStep(Pad *step);

	bool blink;
	void tick();	

	std::vector<unsigned char> screenBuffer;

	bool markUpdateScreen = false;
	void updateScreen();
};
