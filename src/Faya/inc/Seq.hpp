#pragma once

#include "../base64/base64.h"
#include <vector>
#include "Constants.hpp"

#define SEMITONE (1.0/12.0)

struct Seq {
	uint8_t length[SEQ_TRACKS];
	std::vector<uint8_t> seqs[SEQ_TRACKS];
	std::vector<uint8_t> accents[SEQ_TRACKS];

	bool clampNotes = true;

	bool used = false;

	Seq();

	enum SeqType {
		SEQUENCE,
		ACCENT,
	};

	void reset();

	uint8_t toggleSeq(uint8_t track, uint8_t step, SeqType type);

};


struct Memory : Seq {

	Memory();

	Memory(const Seq& s);

	void serialize(std::vector<uint8_t>* data);

	void deserialize(std::vector<uint8_t>* data);
};
