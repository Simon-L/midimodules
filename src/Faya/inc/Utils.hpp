#pragma once

#include <cstdint>

#include "FayaEngine.hpp"

Pad note2Pad(unsigned int note);

char noteToChar(int note);

void PadSysEx(uint8_t x, uint8_t y, uint32_t color, std::vector<unsigned char>* dest);
void SinglePadSysEx(uint8_t x, uint8_t y, uint32_t color, std::vector<unsigned char>* dest);

void ClkSysEx(uint8_t step, uint32_t color, std::vector<unsigned char>* dest);

void ColSysEx(uint8_t step, uint32_t* colColor, std::vector<unsigned char>* dest);

int dimColor(int color);

extern unsigned char SysexStart[5];
void FinishSysEx(std::vector<unsigned char>* dest);

extern uint8_t BitMutate[8][7];

extern uint8_t OLEDBitmap[1175];

void PlotPixel(unsigned x, unsigned y, uint8_t c);

/*void dumpVector(std::vector<unsigned char>* src) {*/
	//for (int i = 0; i < src->size(); i++) {
		//std::cout << "0x" << std::hex << (int)src->at(i) << ", ";
	//}
	//std::cout << std::endl;
	//std::cout << src->size() << std::endl;
/*}*/

void OledSysex(uint8_t *pBitmap, unsigned BitmapLen, std::vector<unsigned char>* dest);
