#pragma once

struct Pad {
	Pad(unsigned int x, unsigned int y, uint32_t color) : x(x), y(y), color(color) {}
	Pad() {}
	unsigned int x;
	unsigned int y;
	uint32_t color;
	unsigned int track = -1; // overflow as init
	unsigned int position = -1; // idem

	uint8_t pagedX(uint8_t page) {
		return this->x + page * 16;
	
	}
};
