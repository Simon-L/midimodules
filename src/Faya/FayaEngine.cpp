#include "inc/FayaEngine.hpp"

#include "dsp/common.hpp"
#include "inc/Constants.hpp"
#include "inc/Utils.hpp"
#include "math.hpp"

FayaEngine::FayaEngine() {
	//this->midiOut = midiOut;

	msg.setStatus(0xb);
	msg.setNote(127);
	msg.setValue(0);

	// 4 knobs per tracks, 8 tracks
	// ((trackSel * 4) + cv) with cv = 0-3, depending on knob
	polyCV.assign(SEQ_TRACKS*4, 0.0);
}

FayaEngine::~FayaEngine() {
	midi::Message m;
	m.setStatus(0xb);
	m.setNote(127);
	m.setValue(0);
	midiOut->sendMessage(m);
	Screen::clear();
	view.updateScreen();

}

void FayaEngine::reset() {
	midi::Message m;
	m.setStatus(0xb);
	m.setNote(127);
	m.setValue(0);
	midiOut->sendMessage(m);

	view.midiOut = midiOut;
	view.seq = &seq;
}

void FayaEngine::init() {
	DEBUG("fayaengine::init");
	seq = memory[currentMemory];

	view.reset(currentMemory);
	//view.showPageSeqs(seq.seqs, index);
	view.lightTransport(running);
	loadMemory(currentMemory);
	view.displayTrack();
	view.displayKnobs(polyCV[view.trackSel * 4],
			  polyCV[view.trackSel * 4 + 1],
			  polyCV[view.trackSel * 4 + 2],
			  polyCV[view.trackSel * 4 + 3]);
	view.displayStep(index, seq.length[0]);
	lightSelect();
}

void FayaEngine::loadMemory(uint8_t id, bool update) {
	// false : Load only the seq, not the view
	// true : load the view, not the seq
	if (id > 63) {
		DEBUG("Memory out of range");
		return;
	}
	// If view is not detached, update seq and view
	if (!view.detached) {
		memory[currentMemory] = seq;
		seq = memory[id];
		currentMemory = id;
		view.displayMemory(currentMemory);
		view.displayStep(index, seq.length[0]);
		view.updateView(index);
	} else {
		if (update) {
			// The seq has been changed by user, change seq because the view will edit it
			// playSeq unchanged
			memory[currentMemory] = seq; // save
			seq = memory[id]; // load to seq, change seq
			currentMemory = id; // don't change current memory because step
			view.displayMemory(currentMemory);
			view.displayStep(index, seq.length[0]);
			view.updateView(index);
		} else {
			// save before changing playSeq if the requested seq is being edited
			if (id == currentMemory) {
				memory[currentMemory] = seq; // save
			}
			playSeq = memory[id]; // change playSeq only
			currentPlayMemory = id; // change playmemory id
		}
	}

	seq.used = true;
}

void FayaEngine::resetSeq() {
	view.moveClock(index, (view.detached ? NO_COL : 0));
	index = 0;
}

midi::Message msg;
void FayaEngine::lightMutes() {
	std::vector<Pad> statePads;
	for (int x = 0; x < 16; ++x)
	{
		statePads.push_back(Pad{x,2,((tracksMute & (1 << x)) ? PERFORM_MUTE_COLOR : PERFORM_MUTE_COLOR_DIM)});
		statePads.push_back(Pad{x,1,((tracksSolo & (1 << x)) ? PERFORM_SOLO_COLOR : PERFORM_SOLO_COLOR_DIM)});
	}
	view.lightMultiplePads(statePads);
}

void FayaEngine::lightSelect(bool off) {
	uint8_t track = 255;
	if (view.trackSel >= view.trackPage * 4 && view.trackSel <= view.trackPage * 4 + 3) {
			track = (view.trackSel - view.trackPage * 4);
			DEBUG("lightSelect track = %x", track);
	}
	for (int t = 0; t < 4; t++) {
		msg.setNote(TRACK_3_BUT + t);
		msg.setValue(((3 - t == track) && !off) ? 0x02 : 0x00);
		midiOut->sendMessage(msg);
	}
	if (view.mode == View::PERFORM) {
		std::vector<Pad> selPads;
		for (int x = 0; x < 16; ++x) {
			selPads.push_back(Pad{x,3,(x == view.trackSel) ? PERFORM_SELECT_COLOR : PERFORM_SELECT_COLOR_DIM});
		}
		view.lightMultiplePads(selPads);
	}
}

void FayaEngine::lightTrackLeds(uint8_t index, bool off = false) {
	if (off) {
		for (int t = TRACK_3_LED; t <= TRACK_0_LED; t++) {
			msg.setNote(t);
			msg.setValue(0x00);
			midiOut->sendMessage(msg);
		}
		return;
	} else {
		uint8_t track;
		for (int t = TRACK_3_LED; t <= TRACK_0_LED; t++) {
			msg.setNote(t);
			track = ((view.trackPage * 4 + 3) - (t - TRACK_3_LED));
			msg.setValue(seq.seqs[track][index] ? 0x03 : 0x00);
			midiOut->sendMessage(msg);
		}
	}
}

bool FayaEngine::toggleTrackMute(uint8_t track) {
	tracksMute ^= (1 << track);
	lightMutes();
	return tracksMute & (1 << track) ? true : false;
}

bool FayaEngine::toggleTrackSolo(uint8_t track) {
	tracksSolo ^= (1 << track);
	lightMutes();
	return tracksSolo & (1 << track) ? true : false;
}

bool FayaEngine::trackState(uint8_t track) {
	if (tracksSolo) return tracksSolo & (1 << track) ? true : false;
	else return tracksMute & (1 << track) ? false : true;		
}

void FayaEngine::lightClk(uint8_t step){
	std::vector<unsigned char> m;
	ClkSysEx(step, CLK_COLOR, &m);
	FinishSysEx(&m);
	midiOut->sendMessage(m.data(), m.size());
}

void FayaEngine::step() {

	// Handle detached view with a pointer to a seq that points to the right one
	Seq* _seq;
	if (view.detached) _seq = &playSeq;
	else _seq = &seq;

	if (!resetIndex) index += 1;
	if (index >= _seq->length[0]) {
		resetIndex = true;
		index = 0;
		EOC = false;
		if (nextSeq != NO_NEXT_SEQ) {
			loadMemory(nextSeq, false);
			nextSeq = NO_NEXT_SEQ;
		}
	}
	view.displayStep(index, _seq->length[0]);

	// EOC
	if ((index == _seq->length[0] - 1) && !resetIndex) {
		EOC = true;
	}
	else EOC = false;

	// UI
	//
	if (!view.detached) {
		if (view.mode != View::PERFORM) lightTrackLeds(index);
		// if the current page contain the current step index
		if (view.indexInView(index)) {
			if (resetIndex) {
				// if we just reset (index is 0),
				// move the clock from the last step in the sequence to 0 if it's in view
				// otherwise just show clock at 0
				if (view.indexInView(_seq->length[0]-1)) view.moveClock(_seq->length[0]-1, index);
				else view.moveClock(NO_COL, index);
			} else {
				// if index is the first step of the shown page,
				// just show the clock at that step
				if (index % 16 == 0) view.moveClock(NO_COL, index);
				// otherwise, just move the clock from the previous step
				// to the current one, ie "index"
				else view.moveClock(index-1, index);
			}
		}
		// otherwise if the current page does NOT show the current clock
		else {
			// if the current step is the first setp of the next page,
			// just update the last 4 steps of the current page
			if (index == (view.page + 1) * 16) view.moveClock(view.page * 16 + 15);
			// if we just reset (index is 0),
			// just update the last step of the sequence
			// but what if the last step is not in view?!
			if (index == 0 && view.indexInView(_seq->length[0]-1)) view.moveClock(_seq->length[0]-1);
			else if (view.mode == View::PERFORM) view.moveClock(NO_COL, index);
		}
	} else {
		if (view.mode == View::PERFORM) {
			view.moveClock(NO_COL, index);
		}
	}

	// Done with resetIndex stuff, reset the reset flag (duh!)
	if (index != _seq->length[0]) resetIndex = false;
}

std::string FayaEngine::encodeMemory(uint8_t id) {
	memory[id].serialize(&buf);
	return base64_encode(buf.data(), buf.size());
}

void FayaEngine::decodeMemory(uint8_t id, const char* str, size_t len) {
	decoded.assign(str, str+len);
	decoded = base64_decode(decoded);
	buf.clear();
	buf.assign(decoded.data(), decoded.data() + decoded.size());
	memory[id].deserialize(&buf);
}

void FayaEngine::handle(midi::Message msg) {


	switch(msg.getStatus()) {
		case BUT_ON :
			if ((msg.getNote() >= FIRST_STEP) && (msg.getNote() <= LAST_STEP)) {
				Pad step = note2Pad(msg.getNote());
				view.pagePad(&step);
				uint8_t state;
				// Handle detached view with a pointer to a seq that points to the right one
				Seq* _seq;
				if (view.detached) _seq = &seq;
				else _seq = &seq;
				switch (view.mode) {
					case View::ACCENT:
					case View::STEP:
						// editSeq
						if (step.position >= _seq->length[0]) break;
						state = _seq->toggleSeq(step.track, step.position, (Seq::SeqType)view.mode);
						if (currentMemory == currentPlayMemory) {
							DEBUG("cur mem == play mem");
							playSeq.toggleSeq(step.track, step.position, (Seq::SeqType)view.mode);
						}
						if (state == 2) step.color = SEQ_ACC_COLOR;
						else step.color = (state ? SEQ_STEP_COLOR : 0x000000);
						view.lightPad(step);
						break;
					case View::PERFORM:
						if (step.y == 3) {
							DEBUG("Perform track select: %d", step.x);
							view.trackSel = step.x;
							view.displayTrack();
							view.displayKnobs(polyCV[view.trackSel * 4],
									  polyCV[view.trackSel * 4 + 1],
									  polyCV[view.trackSel * 4 + 2],
									  polyCV[view.trackSel * 4 + 3]);
							lightSelect();
						}
						if (step.y == 2) {
							DEBUG("Perform track mute: %d", step.x);
							toggleTrackMute(step.x);
						}
						if (step.y == 1) {
							DEBUG("Perform track solo: %d", step.x);
							toggleTrackSolo(step.x);
						}

					default:
						break;
				}
				break;
			}
			uint8_t track;
			switch(msg.getNote()) {
				case SELECT_BUT:
					if (view.browsing) {
						uint8_t copyId = currentMemory;
						uint8_t pasteId = 65;
						for (uint8_t id = copyId + 1; id < 64; ++id) {
							if (memory[id].used == false) {
								pasteId = id;
								DEBUG("pasteId %d", pasteId);
								break;
							}
						}
						if (pasteId == 65) break;
						memory[pasteId].length[0] = seq.length[0];
						for (int t = 0; t < 16; ++t)
						{
							std::copy(seq.seqs[t].begin(), seq.seqs[t].end(), memory[pasteId].seqs[t].begin());
							std::copy(seq.accents[t].begin(), seq.accents[t].end(), memory[pasteId].accents[t].begin());
							DEBUG("size of vector %d", memory[pasteId].seqs[t].size());
						}
						memory[pasteId].used = true;
					}
					break;
				case BRO_BUT :
					view.setBrowsing(true);
					break;
				case TRACK_3_BUT:
				case TRACK_2_BUT:
				case TRACK_1_BUT:
				case TRACK_0_BUT:
					// Disabled for simplicity, will now select tracks
					// track = (view.trackPage ? 7 : 3) - (msg.getNote() - TRACK_3_BUT);
					// if (view.shifted) toggleTrackSolo(track);
					// if (!view.shifted && !tracksSolo) toggleTrackMute(track);
					// else DEBUG("Other case?! %d %x", view.shifted, tracksSolo);
					// New behaviour:
					track = (view.trackPage * 4 + 3) - (msg.getNote() - TRACK_3_BUT);
					DEBUG("Tracksel:  %d", track);
					view.trackSel = track;
					view.displayTrack();
					view.displayKnobs(polyCV[view.trackSel * 4],
							  polyCV[view.trackSel * 4 + 1],
							  polyCV[view.trackSel * 4 + 2],
							  polyCV[view.trackSel * 4 + 3]);
					lightSelect();
					break;
				case SHIFT_BUT:
					view.shifted = true;
					break;
				case PAT_UP_BUT:
					// moveTrackPage returns true if the trackPage has actually moved
					if (view.moveTrackPage(1)) {
						if (view.mode == View::STEP || view.mode == View::ACCENT) view.showPageSeqs(seq.seqs, index);
						lightSelect();
					}
					break;
				case PAT_DO_BUT:
					if (view.moveTrackPage(0)) {
						if (view.mode == View::STEP || view.mode == View::ACCENT) view.showPageSeqs(seq.seqs, index);
						lightSelect();
					}
					break;
				case GRID_L:
					// Set next seq if Browser is held and no next seq already selected
					if (view.browsing && (currentMemory > 0) && (nextSeq == NO_NEXT_SEQ)) {
						nextSeq = currentMemory - 1;
						DEBUG("nextSeq %d", nextSeq);
					} else {
						if (view.movePage(0)) {
							if (view.mode == View::STEP || view.mode == View::ACCENT) view.showPageSeqs(seq.seqs, index);
						}
					}
					break;
				case GRID_R:
					// Set next seq if Browser is held and no next seq already selected
					if (view.browsing && (currentMemory < 63) && (nextSeq == NO_NEXT_SEQ)) {
						DEBUG("nextSeq %d", nextSeq);
						nextSeq = currentMemory + 1;
					} else {
						if (view.movePage(1)) {
							if (view.mode == View::STEP || view.mode == View::ACCENT) view.showPageSeqs(seq.seqs, index);
						}
					}
					break;
				case PLAY_BUT:
					running = !running;
					view.lightTransport(running);
					break;
				case STOP_BUT:
					running = false;
					view.moveClock(index, 0);
					index = 0;
					resetIndex = true;
					view.lightTransport(false);
					break;
				case STEP_BUT:
					if (view.switchMode(View::ACCENT)) {
						view.detached = false;
						view.lightDetached();
						loadMemory(currentMemory);
						view.showPageSeqs(seq.seqs, index);
					}
					break;
				case NOTE_BUT:
					break;
				case PAT_BUT:
					if (view.mode != View::STEP) break;
					view.detached = !view.detached;
					if (view.detached) {
						view.lightDetached();
						lightTrackLeds(index, true);
						playSeq = seq;
						currentPlayMemory = currentMemory;
						view.moveClock(index);
					} else {
						view.lightDetached();
						loadMemory(currentPlayMemory);
						playSeq = Seq();
						currentPlayMemory = 255;
					}
					break;
				case PERF_BUT:
					if (view.switchMode(View::PERFORM)) {
						view.detached = false;
						view.lightDetached();
						loadMemory(currentMemory);
						lightSelect(true);
						lightTrackLeds(index, true);
						view.lightPerformClk(index);
						view.showPerform(index, tracksSolo, tracksMute);
					}
					break;
				case VOL_KNOB:
					break;
				default:
					break;
			}
			break;
		case BUT_OFF:
			switch(msg.getNote()) {
				case STEP_BUT:
					view.switchMode(View::STEP);
					break;
				case SHIFT_BUT:
					view.shifted = false;
					break;
				case BRO_BUT :
					view.setBrowsing(false);
					break;
			}
			break;
		case KNOB:
			uint8_t knob;
			switch(msg.getNote()) {
				case SELECT_KNOB :
					if (msg.getValue() < 64) { // Increment
						// Change length of seq (shift + select)
						if (!running && view.shifted) {
							seq.length[0] = clamp(++seq.length[0], 1, 64);
							if (view.indexInView(seq.length[0]-1)) view.showPage(index);
							view.displayStep(index, seq.length[0]);
						}
						// Change memory (hold browser + select)
						if (!view.shifted && view.browsing) {
							// Load only the view, not the seq
							loadMemory(currentMemory + 1, true);
							DEBUG("Next mem %d", currentMemory);
						}
						// Change track selection
						if (!view.shifted && !view.browsing) {
							view.trackSel = clamp(++view.trackSel, 0, SEQ_TRACKS - 1);
							view.displayTrack();
							view.displayKnobs(polyCV[view.trackSel * 4],
									  polyCV[view.trackSel * 4 + 1],
									  polyCV[view.trackSel * 4 + 2],
									  polyCV[view.trackSel * 4 + 3]);
							lightSelect();
						}
					}
					else { // Decrement
						// Change length of seq (shift + select)
						if (!running && view.shifted) { 
							seq.length[0] = clamp(--seq.length[0], 1, 64);
							if (view.indexInView(seq.length[0])) view.showPage(index);
							view.displayStep(index, seq.length[0]);
						}
						// Change memory (hold browser + select)
						if (!view.shifted && view.browsing) {
							loadMemory(currentMemory - 1, true);
							DEBUG("Prev mem %d", currentMemory);
						}
						// Change track selection
						if (!view.shifted && !view.browsing) {
							view.trackSel = clamp(--view.trackSel, 0, SEQ_TRACKS - 1);
							view.displayTrack();
							view.displayKnobs(polyCV[view.trackSel * 4],
									  polyCV[view.trackSel * 4 + 1],
									  polyCV[view.trackSel * 4 + 2],
									  polyCV[view.trackSel * 4 + 3]);
							lightSelect();
						}
					}
					break;
				case VOL_KNOB:
				case PAN_KNOB:
				case LOEQ_KNOB:
				case HIEQ_KNOB:
					knob = msg.getNote() - VOL_KNOB;
					if (true){
						polyCV[view.trackSel * 4 + knob] = clamp(polyCV[view.trackSel * 4 + knob] + (msg.getValue() < 64 ? 1.0f : -1.0f) * increment, 0.0, 10.0);
						view.displayKnobs(polyCV[view.trackSel * 4],
								  polyCV[view.trackSel * 4 + 1],
								  polyCV[view.trackSel * 4 + 2],
								  polyCV[view.trackSel * 4 + 3]);
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;

	}
}

