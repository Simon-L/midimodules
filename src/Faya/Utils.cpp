#include <cstdint>

#include "inc/FayaEngine.hpp"
#include "inc/Utils.hpp"

// This file is in most part copy-pasted from Paul Curtis blog, hence the different lettercasing/code style https://blog.segger.com/decoding-the-akai-fire-part-1/

uint8_t BitMutate[8][7] = {
  { 13,  19,  25,  31,  37,  43,  49 },
  {  0,  20,  26,  32,  38,  44,  50 },
  {  1,   7,  27,  33,  39,  45,  51 },
  {  2,   8,  14,  34,  40,  46,  52 },
  {  3,   9,  15,  21,  41,  47,  53 },
  {  4,  10,  16,  22,  28,  48,  54 },
  {  5,  11,  17,  23,  29,  35,  55 },
  {  6,  12,  18,  24,  30,  36,  42 }
};

unsigned char SysexStart[] = {0xF0, 0x47, 0x7F, 0x43, 0x65};


// DO NOT DELETE
// v v DO NOT DELETE
uint8_t OLEDBitmap[1175] = {0x00, 0x07, 0x00, 0x7F};	
// ^ ^ DO NOT DELETE

Pad note2Pad(unsigned int note) {
	return Pad((note-0x36)%16, (note-0x36)/16, 0xff0000);
}

char noteToChar(int note) {
	note %= 12;
	switch (note) {
		case 0:
		case 1:
			return 'C';
			break;
		case 2:
			return 'D';
			break;
		case 3:
		case 4:
			return 'E';
			break;
		case 5:
		case 6:
			return 'F';
			break;
		case 7:
		case 8:
			return 'G';
			break;
		case 9:
			return 'A';
			break;
		case 10:
		case 11:
			return 'B';
			break;
		default:
			return 'X';
			break;
	}
}

void PadSysEx(uint8_t x, uint8_t y, uint32_t color, std::vector<unsigned char>* dest) {
	unsigned char _buf[12] = {0xF0, 0x47, 0x7F, 0x43, 0x65, 0x00, 0x04, y*0x10 + x, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F, 0xF7};
	dest->assign(_buf, _buf+12);
};

void SinglePadSysEx(uint8_t x, uint8_t y, uint32_t color, std::vector<unsigned char>* dest) {
	unsigned char _buf[] = {y*0x10 + x, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F};
	dest->insert(dest->end(), _buf, _buf+sizeof(_buf));
}

void ClkSysEx(uint8_t step, uint32_t color, std::vector<unsigned char>* dest) {
	unsigned char _buf[] = {
		0x00 + step, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F,
		0x10 + step, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F,
		2*0x10 + step, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F,
		3*0x10 + step, (color >> 17) & 0x7F, (color >> 9) & 0x7F, (color >> 1) & 0x7F,
	};
	dest->insert(dest->end(), _buf, _buf+sizeof(_buf));
};

void ColSysEx(uint8_t step, uint32_t* colColor, std::vector<unsigned char>* dest) {
	unsigned char _buf[] = {
		0x00 + step, (colColor[3] >> 17) & 0x7F, (colColor[3] >> 9) & 0x7F, (colColor[3] >> 1) & 0x7F,
		0x10 + step, (colColor[2] >> 17) & 0x7F, (colColor[2] >> 9) & 0x7F, (colColor[2] >> 1) & 0x7F,
		2*0x10 + step, (colColor[1] >> 17) & 0x7F, (colColor[1] >> 9) & 0x7F, (colColor[1] >> 1) & 0x7F,
		3*0x10 + step, (colColor[0] >> 17) & 0x7F, (colColor[0] >> 9) & 0x7F, (colColor[0] >> 1) & 0x7F,
	};
	dest->insert(dest->end(), _buf, _buf+sizeof(_buf));
}

void FinishSysEx(std::vector<unsigned char>* dest) {
       /* if (dest->size() > 0x7F) {*/
		//DEBUG("Fix the message size!");
		//return;
	/*}*/
	dest->insert(dest->begin(), dest->size() & 0x7F);
	dest->insert(dest->begin(), dest->size() >> 7);
	dest->insert(dest->begin(), SysexStart, SysexStart + sizeof(SysexStart));
	dest->push_back(0xF7);
}

void PlotPixel(unsigned x, unsigned y, uint8_t c) {
	unsigned RemapBit;
	if (x < 128 && y < 64) {
		// Unwind 128x64 arrangement into a 1024x8 arrangement of pixels.
		x += 128 * (y/8);
		y %= 8;
		// Remap by tiling 7x8 block of translated pixels.
		RemapBit = BitMutate[y][x % 7];
		if (c > 0) {
			OLEDBitmap[4 + x/7*8 + RemapBit/7] |= 1u << (RemapBit % 7);
		} else {
			OLEDBitmap[4 + x/7*8 + RemapBit/7] &= ~(1u << (RemapBit % 7));
		}
	}
	else {
		DEBUG("outside of range! %d %d", x, y);

	}
}

/*void dumpVector(std::vector<unsigned char>* src) {*/
	//for (int i = 0; i < src->size(); i++) {
		//std::cout << "0x" << std::hex << (int)src->at(i) << ", ";
	//}
	//std::cout << std::endl;
	//std::cout << src->size() << std::endl;
/*}*/

void OledSysex(uint8_t *pBitmap, unsigned BitmapLen, std::vector<unsigned char>* dest) {
	dest->insert(dest->begin(), SysexStart, SysexStart+sizeof(SysexStart));
	dest->at(4) = 0x0E; // WRITE OLED command
	dest->push_back(BitmapLen >> 7);
	dest->push_back(BitmapLen & 0x7F);
	//dumpVector(dest);
	dest->insert(dest->end(), pBitmap, pBitmap+BitmapLen);
	//dumpVector(dest);
	dest->push_back(0xF7);
}

