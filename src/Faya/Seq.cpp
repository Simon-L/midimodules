#include "inc/Seq.hpp"

#include "../plugin.hpp"
#include "inc/Utils.hpp"
#include "inc/Constants.hpp"

Seq::Seq() {
	reset();
}

void Seq::reset() {
	for (int i = 0; i < SEQ_TRACKS; i++) {
		length[i] = 16;
		seqs[i].assign(64, 0);
		accents[i].assign(64, false);
	}
};

uint8_t Seq::toggleSeq(uint8_t track, uint8_t step, SeqType type) {
	uint8_t s = seqs[track][step];
	uint8_t a = accents[track][step];
	switch (type) {
		case SEQUENCE:
		case ACCENT:
			if (type != ACCENT) {
				seqs[track][step] = !(s | a);
				accents[track][step] = false;
				return seqs[track][step] ? 1 : 0;
			}
			else {
				seqs[track][step] = !(s & a);
				accents[track][step] = !(s & a);
				return accents[track][step] ? 2 : 0;
			}
			break;
	}
	return 255;
}