#include <cstring>

#include "inc/Seq.hpp"

Memory::Memory() {
};

Memory::Memory(const Seq& s)
	: Seq(s)
{

}

void Memory::serialize(std::vector<uint8_t>* data) {
	data->clear();
	for (int i = 0; i < 8; i++) {
		uint8_t trackLength = length[i];
		data->push_back(trackLength);
		data->insert(data->end(), seqs[i].data(), seqs[i].data() + trackLength);
		data->insert(data->end(), accents[i].data(), accents[i].data() + trackLength);
	}
	return;
}

void Memory::deserialize(std::vector<uint8_t>* data) {
	int cursor = 0;
	for (int i = 0; i < 8; i++) {
		uint8_t trackLength = data->at(cursor);
		length[i] = trackLength;
		cursor++;
		seqs[i].assign(data->data() + cursor, data->data() + cursor + trackLength);
		cursor += trackLength;			
		accents[i].assign(data->data() + cursor, data->data() + cursor + trackLength);
		cursor += trackLength;
	}
}
