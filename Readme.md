# FAYA


### VCV Rack sequencer module for Akai Fire (Rack v1 only)

**/!\ Experimental, full of debug prints stuff! You have been warned, rackhead**
You need a special Rack build for this to work (because of SysEx not being supported *sigh*), until v2 is released (expected november 2021) and I move to the new code where sysex is finally working!
Also, you need Linux, hah!

I chose to split the Faya logic into separate files in case I want to reuse it outside of Rack. That's why everything is inside the `src/Faya` folder but don't expect it to be clean, there are a few dirty tricks for the sake of simplicity and out of laziness.

There's got a lot of programming non-sense in there. An early challenge when I started was what model to use, decoupling UI and module state, which is tricky because the "UI" in this case is both an external device and the module panel in Rack.  
To sum it up I think I can safely say that this will need a solid refactor, the sooner the better.

Font used in the SVG is the Ostrich family.  
All the reverse-engineering work is credited to Paul Curtis: https://blog.segger.com/decoding-the-akai-fire-part-1/

### Alpha dev demo video
https://vimeo.com/523948181

[![Alpha dev demo](https://i.vimeocdn.com/video/1085213614_320.jpg)](https://vimeo.com/523948181 "Faya - Akai Fire in VCV Rack (Alpha dev demo)")
